import 'dart:io';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:image_picker/image_picker.dart';
import 'package:meta/meta.dart';

part 'picture_event.dart';
part 'picture_state.dart';

class PictureBloc extends Bloc<PictureEvent, PictureState> {
  PictureBloc() : super(PictureInitial()) {
    on<ChangeImageEvent>(_onChangeImage);
  }

  void _onChangeImage(PictureEvent event, Emitter emit) async {
    try {
      File? img = await _pickImage();
      if (img != null) {
        emit(PictureSelectedState(picture: img));
      } else {
        emit(
            PictureErrorState(errorMsg: "No se Cargo La imagen correctamente"));
      }
    } catch (err) {
      emit(PictureErrorState(errorMsg: err.toString()));
    }
  }

  Future<File?> _pickImage() async {
    final picker = ImagePicker();
    final XFile? choosenImage = await picker.pickImage(
        source: ImageSource.camera,
        maxHeight: 720,
        maxWidth: 720,
        imageQuality: 85);
    return choosenImage != null ? File(choosenImage.path) : null;
  }
}
