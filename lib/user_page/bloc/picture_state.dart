part of 'picture_bloc.dart';

@immutable
abstract class PictureState extends Equatable {
  const PictureState();
  @override
  List<Object> get props => [];
}

class PictureInitial extends PictureState {}

class PictureLoadingState extends PictureState {}

class PictureSelectedState extends PictureState {
  final File picture;

  PictureSelectedState({required this.picture});
  @override
  List<Object> get props => [picture];
}

class PictureErrorState extends PictureState {
  final String errorMsg;

  PictureErrorState({required this.errorMsg});
  @override
  List<Object> get props => [errorMsg];
}
