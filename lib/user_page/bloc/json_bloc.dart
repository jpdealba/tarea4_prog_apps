import 'dart:convert';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';
import 'package:http/http.dart' as http;
part 'json_event.dart';
part 'json_state.dart';

class JsonBloc extends Bloc<JsonEvent, JsonState> {
  JsonBloc() : super(JsonInitial()) {
    on<ChangeJsonEvent>(_onLoadJson);
  }
  void _onLoadJson(JsonEvent event, Emitter emit) async {
    try {
      var url = Uri.parse(
          'https://api.sheety.co/b528d505922490b4bee81c7278ffbcd5/mockApiTarea4/sheet1');
      var response = await http.get(url);
      Map<String, dynamic> map = json.decode(response.body);
      var data = map["sheet1"] as List<dynamic>;
      emit(JsonLoadedState(cards: data));
    } catch (err) {
      print(err);
      emit(JsonErrorState(errorMsg: err.toString()));
    }
  }
}
