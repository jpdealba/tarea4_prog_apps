part of 'json_bloc.dart';

@immutable
abstract class JsonEvent extends Equatable {
  const JsonEvent();
  @override
  List<Object> get props => [];
}

class ChangeJsonEvent extends JsonEvent {}
// TODO: change this
