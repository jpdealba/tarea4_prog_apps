part of 'json_bloc.dart';

@immutable
abstract class JsonState extends Equatable {
  const JsonState();
  @override
  List<Object> get props => [];
}

class JsonInitial extends JsonState {}

class JsonLoadingState extends JsonState {}

class JsonLoadedState extends JsonState {
  final List<dynamic> cards;
  JsonLoadedState({required this.cards});

  @override
  List<Object> get props => [];
}

class JsonErrorState extends JsonState {
  final String errorMsg;

  JsonErrorState({required this.errorMsg});

  @override
  List<Object> get props => [errorMsg];
}
