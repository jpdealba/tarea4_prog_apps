import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:feature_discovery/feature_discovery.dart';
import 'bloc/json_bloc.dart';
import 'package:path_provider/path_provider.dart';
import 'package:screenshot/screenshot.dart';
import 'package:share/share.dart';
import 'bloc/picture_bloc.dart' as Picture;
import 'circular_button.dart';
import 'cuenta_item.dart';

class Profile extends StatefulWidget {
  Profile({Key? key}) : super(key: key);

  @override
  _ProfileState createState() => _ProfileState();
}

class _ProfileState extends State<Profile> {
  ScreenshotController screenshotController = ScreenshotController();
  Future _captureAndShare() async {
    screenshotController.capture().then((image) async {
      if (image != null) {
        final directory = await getApplicationDocumentsDirectory();
        final imagePath = await File('${directory.path}/image.png').create();
        await imagePath.writeAsBytes(image);
        await Share.shareFiles([imagePath.path]);
      }
    }).catchError((onError) {
      print(onError);
    });
  }

  void initState() {
    BlocProvider.of<JsonBloc>(context).add(ChangeJsonEvent());
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Screenshot(
      controller: screenshotController,
      child: Scaffold(
        appBar: AppBar(
          actions: [
            DescribedFeatureOverlay(
              featureId: 'feature1',
              targetColor: Colors.white,
              textColor: Colors.black,
              backgroundColor: Colors.red.shade100,
              contentLocation: ContentLocation.trivial,
              title: Text(
                'Compartir Pantalla',
                style: TextStyle(fontSize: 20.0),
              ),
              pulseDuration: Duration(seconds: 1),
              enablePulsingAnimation: true,
              overflowMode: OverflowMode.extendBackground,
              openDuration: Duration(seconds: 1),
              description: Text('Este boton sirve para compartir pantalla'),
              tapTarget: Icon(Icons.share),
              child: IconButton(
                tooltip: "Compartir pantalla",
                onPressed: () async {
                  await _captureAndShare();
                },
                icon: Icon(Icons.share),
              ),
            ),
          ],
        ),
        body: Padding(
          padding: EdgeInsets.all(24.0),
          child: SingleChildScrollView(
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                BlocConsumer<Picture.PictureBloc, Picture.PictureState>(
                  listener: (context, state) {
                    if (state is Picture.PictureErrorState) {
                      ScaffoldMessenger.of(context).showSnackBar(
                        SnackBar(
                          content: Text("${state.errorMsg}"),
                        ),
                      );
                    }
                  },
                  builder: (context, state) {
                    if (state is Picture.PictureSelectedState) {
                      return CircleAvatar(
                        backgroundImage: FileImage(state.picture),
                        minRadius: 40,
                        maxRadius: 80,
                      );
                    } else
                      return CircleAvatar(
                        backgroundColor: Colors.grey,
                        minRadius: 40,
                        maxRadius: 80,
                      );
                  },
                ),
                SizedBox(height: 16),
                Text(
                  "Bienvenido",
                  style: Theme.of(context)
                      .textTheme
                      .headline4!
                      .copyWith(color: Colors.black),
                ),
                SizedBox(height: 8),
                Text("Usuario${UniqueKey()}"),
                SizedBox(height: 16),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: [
                    DescribedFeatureOverlay(
                      featureId: 'feature2',
                      targetColor: Colors.white,
                      textColor: Colors.black,
                      backgroundColor: Colors.red.shade100,
                      contentLocation: ContentLocation.trivial,
                      overflowMode: OverflowMode.clipContent,
                      title: Text(
                        'Ver Tarjeta',
                        style: TextStyle(fontSize: 20.0),
                      ),
                      pulseDuration: Duration(seconds: 1),
                      enablePulsingAnimation: true,
                      openDuration: Duration(seconds: 1),
                      description: Text('Este boton sirve para ver la tarjeta'),
                      tapTarget: Icon(Icons.credit_card),
                      child: CircularButton(
                        textAction: "Ver tarjeta",
                        iconData: Icons.credit_card,
                        bgColor: Color(0xff123b5e),
                        action: null,
                      ),
                    ),
                    DescribedFeatureOverlay(
                      featureId: 'feature3',
                      targetColor: Colors.white,
                      textColor: Colors.black,
                      backgroundColor: Colors.red.shade100,
                      contentLocation: ContentLocation.trivial,
                      overflowMode: OverflowMode.clipContent,
                      title: Text(
                        'Cambiar Fotografia',
                        style: TextStyle(fontSize: 20.0),
                      ),
                      pulseDuration: Duration(seconds: 1),
                      enablePulsingAnimation: true,
                      openDuration: Duration(seconds: 1),
                      description:
                          Text('Este boton sirve para cambiar la foto'),
                      tapTarget: Icon(Icons.camera_alt),
                      child: CircularButton(
                        textAction: "Cambiar foto",
                        iconData: Icons.camera_alt,
                        bgColor: Colors.orange,
                        action: () {
                          BlocProvider.of<Picture.PictureBloc>(context)
                              .add(Picture.ChangeImageEvent());
                        },
                      ),
                    ),
                    CircularButton(
                      textAction: "Ver tutorial",
                      iconData: Icons.play_arrow,
                      bgColor: Colors.green,
                      action: () {
                        FeatureDiscovery.discoverFeatures(context, <String>[
                          'feature1',
                          'feature2',
                          'feature3',
                        ]);
                      },
                    ),
                  ],
                ),
                SizedBox(height: 48),
                BlocConsumer<JsonBloc, JsonState>(
                  listener: (context, state) {
                    if (state is JsonErrorState) {
                      ScaffoldMessenger.of(context).showSnackBar(
                        SnackBar(
                          content: Text("${state.errorMsg}"),
                        ),
                      );
                    }
                  },
                  builder: (context, state) {
                    if (state is JsonLoadedState) {
                      if (state.cards.length > 0) {
                        return ListView.builder(
                          shrinkWrap: true,
                          itemCount: state.cards.length,
                          itemBuilder: (BuildContext context, int index) {
                            return CuentaItem(
                              terminacion: state.cards[index]["cvv"].toString(),
                              saldoDisponible:
                                  state.cards[index]["dinero"].toString(),
                              tipoCuenta:
                                  state.cards[index]["cuenta"].toString(),
                            );
                          },
                        );
                      } else {
                        return Text("No hay datos disponibles");
                      }
                      //         ),
                      //       );
                    } else if ((state is JsonErrorState)) {
                      return Text("No hay datos disponibles");
                    } else {
                      return CircularProgressIndicator();
                    }
                    //   } else
                    //     return CircularProgressIndicator();
                  },
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
